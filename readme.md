# 1C platform checker

Механим позволяет получить версию платформы, на которой запущена база, зная только адрес базы.
На некоторых проектах на одном сервере 1С запущено несколько кластеров 1С разных версий. Без дополнительных настроек подключиться через COM объект можно только к базам на одной версии платформы, т.к. регистрируется только 1 COM объект с именем "V83.COMConnector". Решить это можно зарегистрировав в системе COM объект для каждой версии платформы(подробнее [здесь](https://infostart.ru/public/610960/)), с именем в формате Новый ComОбъект("V83.COMConnector_" + НомерВерсии).
Пример использования:
 - есть база получатель, из которой нужно соединиться с базой источником по COM и получить данные
 - у базы источника известен только адрес, но не известна платформа, на которой она работает
 - запрашиваем адрес http://1c-server/1cplatform_checker/?regport=1541, где 1541 - порт кластера базы источника
 - в ответ получаем версию платформы 1С и создаем COM объект Новый ComОбъект("V83.COMConnector_" + НомерВерсии)

1) Установить php на сервер с апачем https://www.php.net/downloads
Скачиваем zip, рампаковываем в C:\Program Files\php
2) Редактируем конфиг апача
http.conf
```
LoadModule php7_module "C:/Program Files/php/php7apache2_4.dll"
PHPIniDir "C:/Program Files/php"

<IfModule dir_module>
    DirectoryIndex index.html index.php
</IfModule>

<IfModule mime_module>
# к имеющимся добавить строки
		
	AddType application/x-httpd-php .php
	AddType application/x-httpd-php-source .phps
</IfModule>

# 1cplatform_checker
Alias "/1cplatform_checker" "C:/Apache24_8.3.10.2561/htdocs/1cplatform_checker/"
<Directory "C:/apache/1cplatform_checker/">
    AllowOverride All
    Options None
    Require all granted
</Directory>
```

3) В C:/Apache24_8.3.10.2561/htdocs/1cplatform_checker положить index.php
4) Настроить запуск апача с опубликованным 1cplatform_checker от Локальная система
5) Тестируем Проверить можно так:
http://1c-server/1cplatform_checker/?regport=1541
